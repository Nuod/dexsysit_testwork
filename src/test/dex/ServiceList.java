package test.dex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ServiceList {

    private List<Long> multOfNumbX; //X = 3
    private List<Long> multOfNumbS; //S = 7
    private List<Long> multOfNumbM; //M = 21
    private Boolean f;

    ServiceList() {
        f = false;
        multOfNumbX = new ArrayList<>();
        multOfNumbS = new ArrayList<>();
        multOfNumbM = new ArrayList<>();
    }

    ServiceList(List<Long> multOfThree, List<Long> multOfSeven, List<Long> multOfTwentOne) {
        this.multOfNumbX = multOfThree;
        this.multOfNumbS = multOfSeven;
        this.multOfNumbM = multOfTwentOne;
    }

    public void init(List<Long> initList) {
        Collections.sort(initList);
        this.clear();
        for (Long itr : initList) {
            boolean mult = false;
            if (itr % Constant.NUMB_M == 0) {
                multOfNumbM.add(itr);
                mult = true;
            }
            if (itr % Constant.NUMB_X == 0) {
                multOfNumbX.add(itr);
                mult = true;
            }
            if (itr % Constant.NUMB_S == 0) {
                multOfNumbS.add(itr);
                mult = true;
            }
            if (!mult) {
                f = true;
            }
        }
    }

    void print() {
        System.out.println(multOfNumbM.isEmpty() ? "List M is empty" : Constant.NUMB_M + " M: " + multOfNumbM);
        System.out.println(multOfNumbS.isEmpty() ? "List S is empty" : Constant.NUMB_S + " S: " + multOfNumbS);
        System.out.println(multOfNumbX.isEmpty() ? "List X is empty" : Constant.NUMB_X + " X: " + multOfNumbX);
    }

    void print(String type) {
        type = type.toLowerCase();
        switch (type) {
            case "x": {
                System.out.println(multOfNumbX.isEmpty() ? "List X is empty" : Constant.NUMB_X + " X: " + multOfNumbX);
                break;
            }
            case "s": {
                System.out.println(multOfNumbS.isEmpty() ? "List S is empty" : Constant.NUMB_S + " S: " + multOfNumbS);
                break;
            }
            case "m": {
                System.out.println(multOfNumbM.isEmpty() ? "List M is empty" : Constant.NUMB_M + " M: " + multOfNumbM);
                break;
            }
            default: {
                System.err.println("Invalid type");
            }
        }
    }

    void clear() {
        multOfNumbX.clear();
        multOfNumbS.clear();
        multOfNumbM.clear();
    }

    void clear(String type) {
        type = type.toLowerCase();
        switch (type) {
            case "x": {
                multOfNumbX.clear();
                break;
            }
            case "s": {
                multOfNumbS.clear();
                break;
            }
            case "m": {
                multOfNumbM.clear();
                break;
            }
            default: {
                System.err.println("Invalid type");
            }
        }
    }

    List<Long> merge() {
        List<Long> mergeList = new ArrayList<>();
        mergeList.addAll(multOfNumbM);
        mergeList.addAll(multOfNumbS);
        mergeList.addAll(multOfNumbX);
        Collections.sort(mergeList);
        this.clear();
        return mergeList;
    }

    public List<Long> getMultOfNumbX() {
        return multOfNumbX;
    }

    public void setMultOfNumbX(List<Long> multOfNumbX) {
        this.multOfNumbX = multOfNumbX;
    }

    public List<Long> getMultOfNumbS() {
        return multOfNumbS;
    }

    public void setMultOfNumbS(List<Long> multOfNumbS) {
        this.multOfNumbS = multOfNumbS;
    }

    public List<Long> getMultOfNumbM() {
        return multOfNumbM;
    }

    public void setMultOfNumbM(List<Long> multOfNumbM) {
        this.multOfNumbM = multOfNumbM;
    }

    public Boolean getF() {
        return f;
    }

    public void setF(Boolean f) {
        this.f = f;
    }

    public void pressF() {
        System.out.println("PayRespect.png");
    }


}
