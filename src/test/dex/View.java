package test.dex;

import java.util.*;

public class View {

    private ServiceList serviceList;

    View() {
        System.err.println("This test work for DexsysIT by Nikita Eliseev\n" +
                "\temail: nuuduha@gmail.com");
        serviceList = new ServiceList();
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Input command: ");
            String command = scanner.nextLine();
            String[] splitCommand = command.split("\\W");
            switch (splitCommand[0]) {
                case "init": {
                    if (splitCommand.length < 2) {
                        System.out.println("Not input array or flag!\nPlease, try again\n");
                        break;
                    }
                    if (splitCommand[1].equals("r")) {
                        Random random = new Random();
                        int size = random.nextInt(30);
                        List<Long> rndList = new ArrayList<>();
                        for (int i = 0; i < size; i++) {
                            rndList.add(random.nextLong() % 150);
                        }
                        System.out.println("Random array: " + rndList);
                        serviceList.init(rndList);

                    } else {
                        List<Long> initList = new ArrayList<>();
                        try {
                            for (int i = 1; i < splitCommand.length; i++) {
                                if (!splitCommand[i].equals("")) {
                                    initList.add(Long.valueOf(splitCommand[i]));
                                }
                            }
                            serviceList.init(initList);
                            System.out.println(initList);
                        } catch (NumberFormatException ex) {
                            System.err.println("Invalid input!Please, check help!");
                        }
                    }
                    break;
                }
                case "print": {
                    if (splitCommand.length < 2) {
                        serviceList.print();
                    } else {
                        serviceList.print(splitCommand[1]);
                    }
                    break;

                }
                case "anymore": {
                    System.out.println(serviceList.getF());
                    break;
                }
                case "clear": {
                    if (splitCommand.length < 2) {
                        serviceList.clear();
                    } else {
                        serviceList.clear(splitCommand[1]);
                    }
                    break;
                }
                case "merge": {
                    List<Long> mergeList = serviceList.merge();
                    System.out.println("Merge list: " + (mergeList.isEmpty() ? "empty" : mergeList));
                    break;
                }
                case "help": {
                    System.out.println("Commands: init flag or array \nList initialization\n " +
                            "flags:\n\tr :random generated array\n\tarray = manual input array of number\n");
                    System.out.println("print type\nDisplay List(s)\ntype(optional):\n\tX, S, M\n");
                    System.out.println("anymore \nDisplays whether the values were not in the list\n");
                    System.out.println("clear type\nClear List(s)\ntype(optional):\n\tX, S, M\n");
                    System.out.println("merge \nMerge all lists into one display and clear all lists\n");
                    System.out.println("exit \nQuit from program\n");
                    break;
                }
                case "pressf": {
                    serviceList.pressF();
                    break;
                }
                case "exit": {
                    System.err.println("Goodbye! See you next time");
                    return;
                }
                default: {
                    System.err.println("Unknown command! Please, check help");;
                }
            }
        }
    }
}

